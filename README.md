#### Optimistic conccurency in Hudi
---
- Note:
```
id | value | partition | timestamp
---+-------+-----------+-----------------------
0  | 1000  | 1         | 2023-03-29 23:39:09.816


Ví dụ, 2 luồng cùng thực hiện ghi vào hudi table với id = 0 và cùng partition 1, 
nhưng value của 2 luồng khác nhau. Ở đây, giả sử luồng A ghi trước và chiếm lock
Luồng A: ghi với record có value = 2000
Luồng B: ghi với record có value = 3000
Khi 2 luồng cùng thực hiện ghi vào hudi table với record có id = 0 và partition = 1 trong cùng 1 khoảng thời gian. 
Lúc này, luồng A đang giữ lock, luồng B cũng cố gắng ghi dữ liệu có id = 0, value = 3000
Tuy nhiên khoảng thời gian giữa các lần ghi giữa 2 luồng này quá ngắn, 
luồng B có thể sẽ ghi record vào hudi trước khi lock của luồng A giải phóng.
Nếu điều này xảy ra, Hudi sẽ báo lỗi về write overlap và luồng B sẽ không thực hiện được ghi.


=> Để tránh trường hợp write overlap, cần đảm bảo rằng khoảng thời gian giữa các lần ghi là đủ lớn để lock được giải phóng 
trước khi luồng thực hiện ghi tiếp theo. Việc cài đặt thời gian chờ lock phù hợp (như thông qua thuộc tính hoodie.write.lock.wait_time_ms) 
và đảm bảo rằng các luồng không ghi vào cùng bản ghi là cách để giải quyết vấn đề này.

```
---
Link to slide: [slide](https://docs.google.com/presentation/d/1_7rxiZ6ffDKwmHOoxGCzfMwsQWMDguluu9Wg4HadcpQ/edit#slide=id.g2106efd55ed_0_20)
