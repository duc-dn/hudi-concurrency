from pyspark import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.types import StringType, DoubleType
from pyspark.sql.functions import sum as _sum, avg, expr, window, from_unixtime, col
from util.logger import logger
from delta.tables import *

from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import StructType, StructField, StringType, TimestampType
import time

# Define the schema of the table
schema = StructType([
    StructField("id", StringType()),
    StructField("value", StringType()),
    StructField("partition", StringType())
])

from config import (
    MINIO_ACCESS_KEY,
    MINIO_SECRET_KEY,
    MINIO_SERVER_HOST,
)

spark = (
    SparkSession.builder.master("local[*]")
    .appName("spark application")
    .config(
        "spark.jars.packages",
        "org.apache.spark:spark-sql-kafka-0-10_2.12:3.1.1,"
        "org.apache.kafka:kafka-clients:2.6.0,"
        "org.apache.spark:spark-token-provider-kafka-0-10_2.12:3.1.1,"
        "org.apache.commons:commons-pool2:2.6.2,"
        "org.apache.spark:spark-avro_2.12:3.1.1,"
        "org.apache.hadoop:hadoop-aws:3.1.1,"
        "com.amazonaws:aws-java-sdk:1.11.271,"
        "org.apache.hudi:hudi-spark3.1-bundle_2.12:0.11.0,"
        "org.apache.hudi:hudi-hive-sync:0.11.0"
    )   
    .config("spark.sql.extensions", "org.apache.spark.sql.hudi.HoodieSparkSessionExtension")
    .config("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
    .config("spark.hadoop.fs.s3a.impl",
            "org.apache.hadoop.fs.s3a.S3AFileSystem")
    .config("spark.hadoop.fs.s3a.access.key", MINIO_ACCESS_KEY)
    .config("spark.hadoop.fs.s3a.secret.key", MINIO_SECRET_KEY)
    .config("spark.hadoop.fs.s3a.endpoint", MINIO_SERVER_HOST)
    .config("spark.hadoop.fs.s3a.path.style.access", "true")
    .config("spark.hadoop.fs.s3a.connection.ssl.enabled", "false")
    .config('spark.hadoop.fs.s3a.aws.credentials.provider',
            'org.apache.hadoop.fs.s3a.SimpleAWSCredentialsProvider')
    .config("spark.hadoop.fs.s3a.connection.maximum", "1000")
    .getOrCreate()
)


spark_sc = spark.sparkContext
spark_sc.setLogLevel('ERROR')


# Define the Hudi options
hudi_options = {
    "hoodie.table.name": "my_hudi_table",
    "hoodie.table.type": "COPY_ON_WRITE",
    "hoodie.datasource.write.operation": 
        "UPSERT",
    "hoodie.datasource.write.recordkey.field": "id",
    "hoodie.datasource.write.partitionpath.field": "partition",
    "hoodie.datasource.write.keygenerator.class": "org.apache.hudi.keygen.SimpleKeyGenerator",
    "hoodie.write.concurrency.mode": "optimistic_concurrency_control",
    "hoodie.cleaner.policy.failed.writes":"LAZY",
    "hoodie.datasource.write.precombine.field": "timestamp",
    "hoodie.write.lock.provider": "org.apache.hudi.client.transaction.lock.ZookeeperBasedLockProvider",
    "hoodie.write.lock.zookeeper.url": "zookeeper:2181",
    "hoodie.write.lock.zookeeper.lock_key": "mykey",
    "hoodie.write.lock.zookeeper.base_path": "/locks-key",
    "hoodie.write.lock.wait_time_ms": "100000",
    "hoodie.write.lock.num_retries": "20",
}

logger.info("="* 20 + "LET'S GO" + "="*20)

# Define the data to be written
data = []

j = 0
partition = 0

# =============== Luong 1 ======================

for i in range(0, 1000):
    if i % 200 == 0:
        partition = partition + 1

    x = (i, f"value {i}", partition)

    data.append(x)

while True:
    logger.info("="* 20 + "START GEN DATA" + "="*20)
    stream_df = spark.createDataFrame(data, schema)
    stream_df = stream_df.withColumn("timestamp", current_timestamp())

    stream_df.show(truncate=False)

    stream_df.write.format("hudi") \
        .options(**hudi_options) \
        .mode("append") \
        .save("s3a://demo/hudi-table")

    logger.info(20*"-" + "STARTING WRITE HUDI TABLE" + "-"*20)
    # Read the data from the Hudi table
    hudi_df = spark.read.format("org.apache.hudi") \
        .load("s3a://demo/hudi-table")
    logger.info(20*"-" + "WRITE HUDI TABLE SUCESSFULLY" + "-"*20)
    logger.info(f"SO record cua hudi table la {hudi_df.count()}")
    hudi_df.select("_hoodie_commit_time", "value").show(n=3, truncate=False)
    logger.info(f"================== TIME SLEEP ==============")
    time.sleep(60)